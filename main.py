import time
import os
from dotenv import load_dotenv

import httplib2
import googleapiclient as apiclient
import googleapiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials
from helpers import month, day_starts_hours, day_ends_hours

CREDENTIALS_FILE = 'credentials.json'
load_dotenv()
work_hours = {}
spreadsheet_id = os.environ.get('SOURCE_TABLE')
dest_spreadsheet_id = os.environ.get('DEST_TABLE')
list_name = os.environ.get('LIST')
first_cell = os.environ.get('FIRST_CELL')
last_cell = os.environ.get('LAST_CELL')
dest_list = os.environ.get('DEST_LIST')

# read keys from file
credentials = ServiceAccountCredentials. \
    from_json_keyfile_name(CREDENTIALS_FILE,
                           ['https://www.googleapis.com/auth/spreadsheets',
                            'https://www.googleapis.com/auth/drive'])

# authorising
httpAuth = credentials.authorize(httplib2.Http())

# select tables editing api, in this case - v4
service = apiclient.discovery.build('sheets', 'v4', http=httpAuth)

# get dates into list
dates_arr = []
header_last_cell = last_cell[:2] + "1"
ranges = [f"{list_name}!A1:{header_last_cell}"]  #

ex = 1
iter = 0
while ex > 0:
    if iter > 5:
        break
    try:
        dates = service.spreadsheets().values().batchGet(spreadsheetId=spreadsheet_id,
                                                       ranges=ranges,
                                                       valueRenderOption='FORMATTED_VALUE',
                                                       dateTimeRenderOption='FORMATTED_STRING').execute()
        ex -= 1
    except:
        print("failed to get data about workdays")
        time.sleep(3)
    iter += 1


sheet_values = dates['valueRanges'][0]['values']
for date in sheet_values[0]:
    if date != '':
        dates_arr.append(date)

print(dates_arr)

ranges = [f"{list_name}!{first_cell}:{last_cell}"]
print(f"обрабатываются данные с {list_name}!{first_cell}:{last_cell}")
ex = 1
iter = 0
while ex > 0:
    if iter > 5:
        break
    try:
        hours = service.spreadsheets().\
            values().\
            batchGet(spreadsheetId=spreadsheet_id, ranges=ranges, valueRenderOption='FORMATTED_VALUE',dateTimeRenderOption='FORMATTED_STRING').\
            execute()
        ex -= 1
    except:
        print("failed to get workdays values")

    iter += 1


sheet_values = hours['valueRanges'][0]['values']
rows_number = 0
# print(sheet_values)
# for every employee iterate over rows with employees
# and get their worktime
for value in sheet_values:
    if type(value) != list:
        continue

    if len(value) == 0:
        continue

    if value[0] != "":
        print("обрабатываю данные таблицы по ", value[0])
        emp_work_hours = {}
        first_idx = 1
        last_idx = 15
        date_idx = 0

        # iterate over workdays in row
        for date in dates_arr:
            day_start = ""
            day_end = ""
            first_not_null_cell_idx = ""
            last_not_null_cell_idx = ""
            day_slots = value[first_idx:last_idx+1]
            slot_idx = 0

            # iterate over work slots in workday
            for slot in day_slots:
                # if slot is not empty
                if slot != '':
                    # check if cell is colored in needed color
                    if first_not_null_cell_idx == '':
                        first_not_null_cell_idx = slot_idx
                    last_not_null_cell_idx = slot_idx
                slot_idx += 1

            # if there are not empty work slots in a day
            if first_not_null_cell_idx != '':
                date_arr = date.split(".")
                date_start = date_arr[0] + "." + month[date_arr[1]] + " " + day_starts_hours[first_not_null_cell_idx]
                date_end = date_arr[0] + "." + month[date_arr[1]] + " " + day_ends_hours[last_not_null_cell_idx]

                # add second day if it was overnight
                if last_not_null_cell_idx == 14:
                    first_day = date_arr[0] + "." + month[date_arr[1]]
                    date_arr[0] = str(int(date_arr[0]) + 1)
                    if len(date_arr[0]) == 1:
                        date_arr[0] = "0" + date_arr[0]
                    date_end_m = date_arr[0] + "." + month[date_arr[1]] + " 00:00"
                    emp_work_hours[first_day] = [date_start, date_end_m, first_not_null_cell_idx,
                                                                              last_not_null_cell_idx]
                    date_end = date_arr[0] + "." + month[date_arr[1]] + " 07:00"
                    date_start_sec = date_end # date_arr[0] + "." + month[date_arr[1]] + " " + day_starts_hours[first_not_null_cell_idx]
                    date_end_sec = date_arr[0] + "." + month[date_arr[1]] + " 07:00"
                    emp_work_hours[date_arr[0] + "." + month[date_arr[1]]] = [date_end_m, date_end, first_not_null_cell_idx,
                                                                              last_not_null_cell_idx]
                rows_number += 1

                month_day = date_arr[0] + "." + month[date_arr[1]]
                if month_day in emp_work_hours:
                    emp_work_hours[month_day][1] = date_end
                else:
                    emp_work_hours[month_day] = [date_start, date_end, first_not_null_cell_idx,
                                                                              last_not_null_cell_idx]

            # increase indexes to select another day
            first_idx += 15
            last_idx += 15
            date_idx += 1
        work_hours[value[0]] = emp_work_hours

# pprint.pprint(work_hours)
# print(rows_number)
filled_cells = []
for employee_name in work_hours:
    if employee_name != "":
        work_days = work_hours[employee_name]
        for work_date in work_days:
            workday_arr = [employee_name, work_date, work_days[work_date][0], work_days[work_date][1]]
            filled_cells.append(workday_arr)


step = 500
first_idx = 0
last_idx = step
if rows_number > step:
    print("разбиваю данные на чанки и публикую в таблицу")
    chunks_no = rows_number // step
    print("всего чанков: " + str(chunks_no))
    rem = rows_number % step
    iter = 0
    while chunks_no > 0:
        try:
            results = service.spreadsheets().values().batchUpdate(spreadsheetId=dest_spreadsheet_id, body={
                "valueInputOption": "USER_ENTERED",
                "data": [
                    {"range": f"{dest_list}!A{2+first_idx}:D{2+last_idx}",
                     "majorDimension": "ROWS",
                     "values": filled_cells[first_idx:last_idx]
                     }
                ]
            }).execute()
            time.sleep(3)
            first_idx += step
            last_idx += step
            iter += 1
            chunks_no -= 1
        except:
            print("unable to push data to the list! " + str(iter))
            time.sleep(3)

    ex = 1
    iter = 0
    # push last chunk
    while ex > 0:
        if iter > 5:
            break
        try:
            results = service.spreadsheets().values().batchUpdate(spreadsheetId=dest_spreadsheet_id, body={
                "valueInputOption": "USER_ENTERED",
                "data": [
                    {"range": f"{dest_list}!A{2 + first_idx}:D{2 + last_idx + rem + 1}",
                     "majorDimension": "ROWS",
                     "values": filled_cells[first_idx:last_idx]
                     }
                ]
            }).execute()
            ex -= 1
        except:
            print(f"{dest_list}!A{2 + first_idx}:D{2 + last_idx + rem + 1}")
            print("unable to publish last chunk to list " + str(iter))
            iter += 1

else:
    while ex > 0:
        if iter > 5:
            break
        try:
            results = service.spreadsheets().values().batchUpdate(spreadsheetId=dest_spreadsheet_id, body={
                "valueInputOption": "USER_ENTERED",
                "data": [
                    {"range": f"{dest_list}!A2:D{rows_number}",
                     "majorDimension": "ROWS",
                     "values": filled_cells
                     }
                ]
            }).execute()
        except:
            print("unable to publish last chunk to list " + str(iter))
            iter += 1

print("успешно обработал данные и сохранил в таблицу")
print(f"https://docs.google.com/spreadsheets/d/{dest_spreadsheet_id}")