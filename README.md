##создать виртуальную среду
(можно не делать, но желательно)
```shell
    python -m venv venv
    source venv/activate
```

## установить зависимости
```shell
    python -m pip install -r requirements.txt
```

## задать настройки в файле .env
```python
    SOURCE_TABLE = 137W3Ogz1DNjUKmPRJzjrBWco4RDF3g-jLOw33Kpk9CI # исходная таблица с данными
    LIST = Лист1          # название листа с данными
    FIRST_CELL = A4       # первая ячейка диапазона
    LAST_CELL = HR150     # последняя ячейка диапазона
    
    DEST_TABLE = 1n6zhcufm56GUl43f0qqnWYLWrI31800Xusngdbn8HRY  # таблица для вставки
    DEST_LIST = PreparedData                                   # название листа для вставки
```
## запустить скрипт
```shell
    python main.py
```
